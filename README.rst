=========================================================================
 check-links, a simple script to check for dead links in static websites
=========================================================================

check-links is a very simple script to check for dead links (404 not
found) in a static website locally. It mainly relies on
``wget --spider``, but unfortunately ``wget`` cannot walk recursively
through the filesystem, hence it can only check links in a live
website. check-links runs a directory containing a static website
through Python's http.server, and runs ``wget --spider`` on the
result.

Example usage::

  # Check links on index.html in the current directory:
  check-links.py

  # Check links on foo.html and bar.html:
  check-links.py --page foo.html --page bar.html

  # Run a custom command while the site is being served:
  check-links.py --port 8910 -- wget --spider http://localhost:8910

Install
=======

Just download the file ``check-links.py``. It's self-contained and has
no dependency other than Python and its standard library.

Alternatively, you may run::

  pip3 install git+https://gitlab.com/moy/check-links

Note that check-links requires Python 3 (for its ``http.server``
module).
