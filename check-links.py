#! /usr/bin/env python3

"""Check links locally in a static website.

The job is mainly done by wget --spider, but this command cannot run
with file:// nor using plain filenames, hence we run a local webserver
in Python and run wget on http://localhost/.
"""

# BSD 2-Clause License
#
# Copyright (c) 2017, Matthieu Moy
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# * Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in
#   the documentation and/or other materials provided with the
#   distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from http.server import HTTPServer, SimpleHTTPRequestHandler
import subprocess
import threading
import sys
import argparse

__version__ = '0.9'


def start_server(port,
                 server_class=HTTPServer,
                 handler_class=SimpleHTTPRequestHandler):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    thread = threading.Thread(target=httpd.serve_forever)
    thread.start()
    return httpd


def check_page(page):
    url = "http://localhost:{}/{}".format(args.port, page)
    cmd = ["wget",
           "--spider", "-nd",  # just check the page existance, don't download
           "-nv",  # Don't be too verbose
           "-H",  # Check links to other hosts
           "-r",  "-l", "1",  # Follow links, but just once
           url]
    try:
        status = subprocess.call(cmd)
    except KeyboardInterrupt:
        server.shutdown()
        sys.exit(1)
    if status > 0:
        print("Error found on page", page)
        server.shutdown()
        sys.exit(status)


def main(cmd, port,
         server_class=HTTPServer,
         handler_class=SimpleHTTPRequestHandler):
    httpd = start_server(port)
    status = subprocess.call(cmd)
    httpd.shutdown()
    sys.exit(status)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", "-p",
                        help="TCP port number to use",
                        default="8080",
                        metavar="PORT", type=int)
    parser.add_argument("--page",
                        action='append',
                        default=[],
                        help="Page to check. "
                             "Repeat option to check several pages.",
                        metavar="PAGE", type=str)
    parser.add_argument("CMD", nargs='*',
                        default=[],
                        help="Command to run "
                             "(default: wget --spider ... for each page)",
                        type=str)
    args = parser.parse_args()
    if not args.CMD:
        server = start_server(args.port)
        if args.page:
            for page in args.page:
                check_page(page)
        else:
            check_page('')
        server.shutdown()
    else:
        main(args.CMD, args.port)
