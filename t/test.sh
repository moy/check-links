#!/bin/sh

die() {
    echo "$*"
    exit 1
}

rst2html=rst2html
for attempt in rst2html5.py rst2html.py; do
    if command -v $rst2html; then
	break
    else
	rst2html="$attempt"
    fi
done


set -x

for page in *.rst; do
    $rst2html $page > ${page%.rst}.html
done

../check-links.py --page foo.html && die "Broken link not found in foo.html"
../check-links.py --page external-ok.html || die "Spurious error in external-ok.html"
../check-links.py --page external-ko.html && die "External broken link not found in external-ko.html"
../check-links.py || die "Failed without argument"
../check-links.py true || die "true failed"
../check-links.py false && die "false succeeded"
../check-links.py --port 8910 -- wget --spider http://localhost:8910 || die "Did not run properly on non-standard port"
../check-links.py --page index.html --page external-ok.html || die "Did not run properly on multiple pages"
../check-links.py --page index.html --page foo.html && die "Did not detect errors on multiple pages"

echo "All tests passed :-)"

exit 0
